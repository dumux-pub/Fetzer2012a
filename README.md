Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

T. Fetzer<br>
Numerical analysis of the influence of turbulence on exchange processes between porous-medium and free flow<br>
Diploma Thesis, 2012<br>
Universität Stuttgart

You can use the .bib file provided [here](Fetzer2012a.bib).


Installation
============

**Pleas note:**
The script only retrieves the necessary code bases.
Compiling executables will not work due to issues in automake and SuperLU (tried on 2017-11-16).

The easiest way to install this module is to create a new folder and to execute the file
[installFetzer2012a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2015a/raw/master/installFetzer2012a.sh)
in this folder.

```bash
mkdir -p Fetzer2012a && cd Fetzer2012a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2012a/raw/master/installFetzer2012a.sh
sh ./installFetzer2012a.sh
