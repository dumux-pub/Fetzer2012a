#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

### Clone the necessary modules
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.0 https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone -b releases/2.3 https://gitlab.dune-project.org/staging/dune-typetree.git
git clone -b v3.12.1 https://gitlab.dune-project.org/staging/dune-uggrid.git external/ug-3.12.1
git clone -b releases/2.3 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone git@git.iws.uni-stuttgart.de:dumux-repositories/dumux-devel-old.git dumux-devel
cd dumux-devel && git checkout 248d5967a1932dd4cf794e19fab213da148ef0b7 && cd ..
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2012a.git dumux-Fetzer2012a

### EXTERNAL MODULES: UG is required
cd external/ug-3.12.1
autoreconf -is
OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
# debug flags
if test "$ENABLE_DEBUG" == "y"; then
    OPTIM_FLAGS="-O0 -g2"
fi
CFLAGS="$OPTIM_FLAGS"
CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
OPTS="--enable-dune --prefix=$PWD"

if test "$ENABLE_MPI" == "y"; then
    OPTS="$OPTS --enable-parallel MPICC=$MPICXX"
else
    OPTS="$OPTS --without-mpi"
fi

./configure \
    CC=g++ \
    CFLAGS="$CFLAGS" \
    CXXFLAGS="$CXXFLAGS" \
    $OPTS
make
make install

### run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" dumux-Fetzer2012a/automake.opts > dumux-Fetzer2012a/automake_used.opts
./dune-common/bin/dunecontrol --opts=dumux-Fetzer2012a/automake_used.opts all
